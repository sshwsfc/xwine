#coding=utf-8
from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext as _
from sorl.thumbnail.fields import ImageWithThumbnailsField
import uuid

class UserBaseModel(models.Model):
    user = models.ForeignKey(User, verbose_name="经销商")
    
    class Meta:
        abstract = True
    
class Customer(UserBaseModel):

    def __unicode__(self):
        return "%s" % self.user.username

    class Meta:
        verbose_name = u'用户信息'
        verbose_name_plural = verbose_name
        
class Category(UserBaseModel):
    name = models.CharField(verbose_name=u"名字", max_length=64)
    description = models.CharField(verbose_name=u"描述", max_length=256, null=True, blank=True)
    sort = models.IntegerField(verbose_name=u"排序升序", default=0, null=True, blank=True)

    icon = ImageWithThumbnailsField(_(u"图标"), upload_to=lambda app, name: "images/category/%s.%s" % (
        uuid.uuid4(), name.split('.')[-1]), blank=False, null=False,
                                    thumbnail={'size': (48, 48), 'extension': 'jpg'},
                                    extra_thumbnails={#'options': ['crop', 'upscale']
                                                      'list': {'size': (34, 34), 'extension': 'jpg'},
                                                      'phone': {'size': (100, 100), 'extension': 'jpg'},
                                                      'full': {'size': (300, 300), 'extension': 'jpg'},
                                                      }, )

    def __unicode__(self):
        return "%s" % (self.name,)

    class Meta:
        verbose_name = u'产品分类'
        verbose_name_plural = verbose_name
        
class Product(UserBaseModel):
    name = models.CharField(u"产品名称", max_length=64)
    slug = models.CharField(u'产品短描述', max_length=200, null=True, blank=True)
    introduction = models.TextField(u'产品介绍', null=True, blank=True)
    
    category = models.ForeignKey(Category, verbose_name="产品类别", related_name="products")
    
    company = models.CharField(u"产品厂家", max_length=64, null=True, blank=True)
    year = models.IntegerField(u'出产年份')

    publish_time = models.DateTimeField(verbose_name=u"发布起始时间", null=True, blank=True)
    publish_time.db_index = True
    video_url = models.CharField(verbose_name=u"视频介绍地址", max_length=255, null=True, blank=True)

    logo = ImageWithThumbnailsField(_(u"图标"), upload_to=lambda app, name: "images/app/logo/%s.%s" % (
        uuid.uuid4(), name.split('.')[-1]), blank=False, null=False,
                                    thumbnail={'size': (48, 48), 'extension': 'jpg'},
                                    extra_thumbnails={#'options': ['crop', 'upscale']
                                                      'list': {'size': (34, 34), 'extension': 'jpg'},
                                                      'phone': {'size': (100, 100), 'extension': 'jpg'},
                                                      'full': {'size': (300, 300), 'extension': 'jpg'},
                                                      }, )

    comment_count = models.IntegerField(default=0, null=True, blank=True, verbose_name="评论数")
    favorite_count = models.IntegerField(default=0, verbose_name="收藏数")
    level = models.IntegerField(default=0, verbose_name="星级(1-10)")

    class Meta:
        verbose_name = u'产品信息'
        verbose_name_plural = verbose_name
        ordering = ['-publish_time',]

    def __unicode__(self):
        return u"%s" % (self.name)

PIC_TYPE = (
    ('1', '展示'),
    ('2', '瓶身'),
    ('3', '相关'),
    )

class ProductPic(models.Model):
    product = models.ForeignKey(Product, related_name="pics")
    type = models.CharField(u'图片类别', max_length=10, choices=PIC_TYPE)
    image = ImageWithThumbnailsField(
        upload_to=lambda ss, name: "images/product/%s.%s" % (uuid.uuid4(), name.split('.')[-1]), blank=False,
        null=False,
        thumbnail={'size': (320, 480), 'extension': 'jpg'},
        extra_thumbnails={
            'phone': {'size': (360, 330), 'extension': 'jpg'},
            '2xphone': {'size': (720, 660), 'extension': 'jpg'},
        }, )
    sort = models.IntegerField(u'排序', default=0)
        
    class Meta:
        verbose_name = u'产品图片'
        verbose_name_plural = verbose_name
        ordering = ['sort',]

    def __unicode__(self):
        return u"%s 图片" % (self.product.name)

class PriceType(UserBaseModel):
    name = models.CharField(u'价格类别名称', max_length=32)
    groups = models.ManyToManyField(Group, verbose_name=u'可查看用户组')
    
    class Meta:
        verbose_name = u'价格类别'
        verbose_name_plural = verbose_name
        
    def __unicode__(self):
        return self.name

class ProductPrice(models.Model):
    product = models.ForeignKey(Product, related_name="prices")
    type = models.ForeignKey(PriceType, verbose_name="价格类别", related_name="prices")
    price = models.IntegerField(u'产品价格')
    
    class Meta:
        verbose_name = u'产品价格'
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return u"%s %s 价格: %s" % (self.product.name, self.type.name, self.price)
    
    
#国家：法国
#品牌：香奈
#箱规：1*6
#酒精度：13%Vol
#净含量：750ml
#产地：法国 奥克
#参考年份：2011年
#色泽：宝石红色
#香气：馥郁的樱桃，黑加仑，青椒，甘草和香料的气息。
#口感：圆润
#酒体：平衡
#瓶塞：橡木塞
#酿造工艺：采摘成熟的葡萄，去梗破碎后控温发酵6-12小时，轻柔加压。为了保留丰富果香，用不锈钢桶进行陈酿。
#灌装日期：2012-04-12
#储藏条件：18~20℃，避光储存，平卧摆放。
#搭配美食：各种卤汁料理，野味和辣菜、酱鸭、鹅肝、红烧肉搭配。
#建议醒酒时间：30分钟
#最佳享用期：2013年-2023年
#原料配料：赤霞珠、西拉
#奖项荣誉：2008年，国家地区餐酒大赛银奖。
#等级：IGP
INFOS = (
    ('1', '国家'),
    ('2', '品牌'),
    ('3', '箱规'),
    ('4', '酒精度'),
    ('5', '净含量'),
    ('6', '产地'),
    ('7', '参考年份'),
    ('8', '色泽'),
    ('9', '香气'),
    ('10', '口感'),
    ('11', '酒体'),
    ('12', '瓶塞'),
    ('13', '酿造工艺'),
    ('14', '灌装日期'),
    ('15', '储藏条件'),
    ('16', '搭配美食'),
    ('17', '建议醒酒时间'),
    ('18', '最佳享用期'),
    ('19', '原料配料'),
    ('20', '奖项荣誉'),
    ('21', '等级'),
)

class ProductInfo(models.Model):
    product = models.ForeignKey(Product, related_name="infos")
    name = models.CharField(u'信息名称', max_length=32, choices=INFOS)
    value = models.CharField(u'信息内容', max_length=128)
    
    class Meta:
        verbose_name = u'产品信息'
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return u"%s %s:%s" % (self.product.name, self.name, self.value)

class Adv(UserBaseModel):
    product = models.ForeignKey(Product, related_name="advs")
    pic = ImageWithThumbnailsField(_(u"图片"), upload_to=lambda app, name: "images/app/adv/%s.%s" % (
        uuid.uuid4(), name.split('.')[-1]), blank=False, null=False,
        thumbnail={'size': (360, 120), 'extension': 'jpg'},
        extra_thumbnails={'phone': {'size': (360, 145), 'options': ['crop', 'upscale'], 'extension': 'jpg'},
                          'full': {'size': (360, 145), 'extension': 'jpg'},
                          }, )
    sort = models.IntegerField(u'排序', default=0)
    
    class Meta:
        verbose_name = u'产品展示'
        verbose_name_plural = verbose_name
        ordering = ['sort']

    def __unicode__(self):
        return u"%s 展示" % self.product.name

ORDER_STATUS = (
    (0, '已下单'),
    (1, '已确认'),
    (2, '已发货'),
    (3, '已签收'),
    (4, '已完成'),
    (5, '缺货'),
    (6, '未知问题'),
)

class Order(UserBaseModel):
    order_time = models.DateTimeField(verbose_name=u"下单时间", auto_now=True)
    uuid = models.CharField(u'买家ID', max_length=128)
    amount = models.IntegerField(u'订单总金额', default=0)
    address = models.CharField(u'送货地址', max_length=300)
    contact = models.CharField(u'联系方式', max_length=20)
    name = models.CharField(u'客户姓名', max_length=10)
    comment = models.CharField(u'客户留言', max_length=300, null=True, blank=True)

    status = models.IntegerField(u'订单状态', default=0, choices=ORDER_STATUS)
    moeo = models.CharField(u'订单备注', max_length=400, null=True, blank=True)

    class Meta:
        verbose_name = u'产品订单'
        verbose_name_plural = verbose_name
        ordering = ['order_time']

    def __unicode__(self):
        return u"%s %s" % (self.name, self.order_time)

class Good(models.Model):
    order = models.ForeignKey(Order, verbose_name=u'订单', related_name='goods')
    product = models.ForeignKey(Product, verbose_name=u'产品')
    price = models.IntegerField(u'交易价格')
    count = models.IntegerField(u'购买数量', default=1)
    discount = models.IntegerField(u'折扣(百分比)', default=100)

    def total_price(self):
        return int(self.price * self.discount * self.count / 100)

    class Meta:
        verbose_name = u'订单产品'
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return u"%s %s" % (self.order, self.product)


