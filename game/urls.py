from django.conf.urls.defaults import patterns, url
from views import item, items, categorys, advs, order

urlpatterns = patterns('',
    url(r'^advs$', advs, name='advs'),
    url(r'^items/(?P<mode>\w+)$', items, name='items'),
    url(r'^item', item, name='item'),
    url(r'^categorys$', categorys, name='categorys'),
    url(r'^order$', order, name='order'),
)
