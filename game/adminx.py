#coding=utf-8
import xadmin as admin
from xadmin.layout import Main, Fieldset, Side
from xadmin.plugins.inline import Inline

from models import UserBaseModel, Product, Category, ProductPic, ProductInfo, Adv, PriceType, ProductPrice, Order, Good
import plugin

class MainDashboard(object):
    widgets = [
        [
            {"type": "html", "title": "欢迎", "content": "<h3> 欢迎使用产品发布平台管理系统！！ </h3>"},
            {"type": "qbutton", "title": "快速开始", "btns": [{'model': Product}, {'model':Category}, {'model':Order}]},
        ],
        [
            {"type": "list", "model": Product, 'params': {'o':'-publish_time'}},
        ]
    ]
admin.site.register(admin.views.website.IndexView, MainDashboard)

class GolbeSetting(object):
    site_title = "产品管理系统"
    globe_search_models = [Product,]
admin.site.register(admin.views.CommAdminView, GolbeSetting)

class BaseCustomerAdmin(object):
    
    def queryset(self):
        qs = super(BaseCustomerAdmin, self).queryset()
        if not self.user.is_superuser and issubclass(self.model, UserBaseModel):
            return qs.filter(user=self.user)
        else:
            return qs            

class CategoryAdmin(BaseCustomerAdmin):

    def icon_show(self, a):
        return "<a href='%s' target='_blank'><img src='%s' style='height: 50px;'/></a>" % (a.icon.url, a.icon.url)
    icon_show.short_description = "类别图片"
    icon_show.allow_tags = True
    icon_show.is_column = False
    
    def ps_count(self, c):
        return c.products.count()
    ps_count.short_description = "产品数量"
    ps_count.is_column = True

    list_display = ('icon_show', 'name', 'description', 'ps_count')
    list_display_links = ('name',)
    search_fields = ['name']
    style_fields = {'products': 'm2m_transfer'}
    model_icon = 'folder-open'

class ProductPicInline(object):
    model = ProductPic
    extra = 2
    
class ProductPriceInline(object):
    model = ProductPrice
    extra = 1
    style = 'table'
    
class ProductInfoInline(object):
    model = ProductInfo
    extra = 4
    style = 'table'
    
class PriceTypeAdmin(BaseCustomerAdmin):
    list_display = ('name', 'groups')
    list_display_links = ('name',)
    search_fields = ['name']
    style_fields = {'groups': 'm2m_transfer'}
    model_icon = 'money'
    
class ProductAdmin(BaseCustomerAdmin):
    list_display = ('name', 'category', 'slug', 'level', 'publish_time',)
    list_display_links = ('name',)
    search_fields = ['name']
    list_filter = ["level", 'publish_time', 'category']
    relfield_style = 'fk-ajax'
    reversion_enable = True
    model_icon = 'glass'
    
    form_layout = (
        Main(
            Fieldset('',
                'name', 'slug', 'category', 'introduction', 'level', 'publish_time', 'logo',
                css_class='unsort short_label no_title'
            ),
            Inline(ProductPrice),
            Inline(ProductPic),
            Inline(ProductInfo),
        ),
        Side(
            Fieldset('数据统计',
                'comment_count', 'favorite_count'
            ),
        )
    )
    inlines = [ProductPicInline, ProductInfoInline, ProductPriceInline]
    ordering = ("-publish_time", "-level",)
    
class GoodInline(object):
    model = Good
    extra = 0
    style = 'table'
    #readonly_fields = ('product', 'price', 'count', 'discount')

class OrderAdmin(BaseCustomerAdmin):
    list_display = ('order_time', 'name', 'contact', 'amount', 'status', 'comment')
    list_display_links = ('order_time',)
    search_fields = ['name', 'contact', 'address']
    list_filter = ["order_time", 'amount', 'status']
    reversion_enable = True
    model_icon = 'shopping-cart'
    exclude = ('uuid',)

    readonly_fields = ('order_time', 'name', 'amount', 'address', 'contact', 'comment')
    
    form_layout = (
        Fieldset('订单信息', *readonly_fields),
        Inline(Good),
        Fieldset('订单处理', 'status', 'moeo'),
    )
    inlines = [GoodInline,]

class AdvAdmin(BaseCustomerAdmin):

    def pic_show(self, a):
        return "<a href='%s' target='_blank'><img src='%s' style='height: 50px;'/></a>" % (a.pic.url, a.pic.url)
    pic_show.short_description = "广告图片"
    pic_show.allow_tags = True
    pic_show.is_column = False

    list_display = ('id', 'pic_show', 'product', 'sort')
    list_display_links = ('id',)
    search_fields = ['product__name']
    model_icon = 'picture'
    
admin.site.register(Product, ProductAdmin)
admin.site.register(Adv, AdvAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(PriceType, PriceTypeAdmin)
admin.site.register(Order, OrderAdmin)

