#coding = utf-8

def rotate_img(im, requested_size, opts):

    #print " * " * 20
    degree = opts.get("rotate")
    #print "degree", degree
    if degree and degree != 0:
        return im.rotate(degree)
    else:
        return im
rotate_img.valid_options = ('rotate',)