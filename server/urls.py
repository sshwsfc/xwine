from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
import xadmin
xadmin.autodiscover()

from xadmin.plugins import xversion
xversion.registe_models()

from game import urls

urlpatterns = patterns('',
    url(r'xadmin/', include(xadmin.site.urls)),
    url(r'api/', include(urls)),
)
