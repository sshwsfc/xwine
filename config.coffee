path = require 'path'
fs = require 'fs'

ext_src = path.join path.dirname(fs.realpathSync __filename), '../miwa/lib/exts'
{languages, plugins} = require ext_src

exports.config =
  plugins: [plugins.AssetsPlugin]
  files:
    'scripts/app.js':
      languages:
        '\.js$': languages.JavaScriptLanguage
        '\.coffee$': languages.CoffeeScriptLanguage
        '\.eco$': languages.EcoLanguage
        '\.js\.html$': languages.JSTemplateLanguage
      order:
        before: [
          'vendor/scripts/console-helper.js'
          'vendor/scripts/zepto.js'
          'vendor/scripts/underscore.js'
          'vendor/scripts/backbone.js'
          'vendor/scripts/backbone.localStorage.js'
          'vendor/scripts/miwa.js'
          'vendor/scripts/miwa.page.js'
          'vendor/scripts/iscroll.js'
          'vendor/scripts/swipe.js'
        ]

    'styles/app.css':
      languages:
        '\.css$': languages.CSSLanguage
        '\.styl$': languages.StylusLanguage
      order:
        before: [
          #'vendor/styles/bootstrap.min.css'
          'app/styles/layout.styl'
        ]

    'styles/app2x.css':
      languages:
        '\.2x\.css$': languages.CSSLanguage
        '\.2x\.styl$': languages.StylusLanguage
