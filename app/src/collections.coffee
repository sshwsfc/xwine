$ma.coll ()->

  Collections:
    pageSize: 15

    initialize : (@options={})->

    parse: (resp, xhr)->
      if @total isnt resp.total_count
          @total = resp.total_count
          @trigger 'change:total', @, @total
      resp.items or []

    fetch: (options)->
      options.data = _.extend options.data or {}, @options.data or {}, {count: @pageSize}
      $colls.Base.prototype.fetch.call @, options

    nextPage: (options)->
      if typeof @total is "undefined"
        @total = @models.length
      if @total > @models.length
        @fetch _.extend {
          add: true
          forceRefresh: true
          data:
            start: @models.length
            count: @pageSize
        }, options
      else
        @trigger 'page_end', @

  Advs:
    model: $models.Adv
    url: '/advs'

  # 推荐产品
  HomeItems$Collections:
    model: $models.Item
    url: '/items/feature'
    
  # 产品分类
  CategoryItems$Collections:
    model: $models.Item
    url: '/items/category'

    category: (cid)->
      @options.data ?= {}
      @options.data['category_id'] = cid
      
  # 类别列表
  Categories$Collections:
    model: $models.Category
    url: '/categorys'
    
  # 产品搜索
  SearchItems$Collections:
    model: $models.Item
    url: '/items/search'

    search: (keyword)->
      @options.data ?= {}
      @options.data['name'] = keyword

  Goods:
    model: $models.Good
    localStorage: new Backbone.LocalStorage("goods-xwine")

    sync: ()->
      return Backbone.localSync.apply @, arguments

    clean: ()->
      @localStorage._clear()
      @reset()

    submit: (data)->
      goods = for good in @models
        "#{good.get('project_id')}|#{good.get('price_id')}|#{good.get('count')}"
      data += '&goods=' + (goods.join ';') + '&uuid=' + $.user_id() + '&csrftoken=' + $.cookie 'csrftoken'

      $.ajax 
        url: window['api_base']+'/order'
        data: data
        success: (data, status, xhr)=>
          if data['success']
            @trigger 'submit.success', data
          else
            @trigger 'submit.error', data['error']
        error: (xhr, status, error)=>
          @trigger 'submit.error', error
        type: 'GET'
        dataType: 'json'
        windowname: window['windowname']
