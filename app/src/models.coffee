$ma.model ()->
  Adv: {}
  Category: {}
  Item:
    url: ()->
      '/item?id=' + @id
    parse: (resp, xhr)->
      resp.star_count = Math.floor(resp.level / 2)
      resp.unstar_count = 5 - resp.star_count
      resp
  Good: 
    sync: ()->
      return Backbone.localSync.apply @, arguments