$ma.view ()->

  MainPage$Page:
    #static methods
    $pagePath: 'main'
    transition: 'none'

    make_views: ()->
      top_banner: new $views.TopBanner {}
      tab_container: new $views.TabContainer @main_tab()

    nav: ()->
      @views.tab_container.nav @params
      @views.top_banner.set_title '晓明品酒'

    main_tab: ()->
      tabs: 
        home: '推荐', category: '分类', search: '搜索'
      views: (nav)=> @main_tab_view nav
      default: 'home'
      page: @

    main_tab_view: (tabnav)->
      switch tabnav
        when "home" then new $views.Home
        when "category" then new $views.Categorys
        when "search" then new $views.Search {page: @}

  CategoryPage$Page:
    $pagePath: 'category'
    transition: 'none'

    make_views: ()->
      top_banner: new $views.TopBanner
      items: new $views.CategoryItems

    nav: ()->
      @views.items.category @params['id']
      @views.top_banner.set_title @params['name']

  SearchPage$Page:
    $pagePath: 'search'
    transition: 'none'

    make_views: ()->
      top_banner: new $views.TopBanner
      items: new $views.SearchItems

    nav: ()->
      keyword = @params['keyword']
      @views.top_banner.set_title "搜索 '#{keyword}'"
      @views.items.search keyword
      
  Home$Container:

    make_views: ()->
      banner: new $views.HomeBanner
      items: new $views.FeatureItems

  Search$Container:

    initialize : ()->
      $views.Container.prototype.initialize.apply @, arguments
      @views.bar.bind 'search', @search, @
      @page = @options.page

    make_views: ()->
      bar: new $views.SearchBar
    
    search: (keyword)->
      @page.navigate "#search?keyword=#{keyword}", on

  GameInfo$Page:

    $pagePath: 'game'
    transition: 'none'

    initialize : (options)->
      $views.Page.prototype.initialize.call @, options
      @bind 'show', ()->
        if @game.unfetched
          @game.fetch {success: _.bind @finishFetch, @}

    finishFetch: (model, resp)->
      @views.info.finishFetch model, resp
      @game.unfetched = false

    nav: ()->
      @views.top_banner.set_title "商品详情"
      new_game = new $models.Item {id: @params['id'], name: @params['name']}
      if @game == undefined or @game.id + '' != new_game.id
        @game = new_game
        @game.unfetched = true
        @views.info.set_game @game
        
    make_views: ()->
      top_banner: new $views.TopBanner
      info: new $views.ItemView

  ScreenShot$Page:

    $pagePath: 'ss'
    transition: 'none'

    nav: ()->
      @views.image.set_img @params['img']

    make_views: ()->
      image: new $views.ImageView
      #backbtn: new $views.BackBtn

  ShopCart$Page:

    $pagePath: 'cart'
    transition: 'none'

    make_views: ()->
      top_banner: new $views.TopBanner {'title':'结算台', 'nocart': true}
      goods: new $views.CartGoods

  ShopBuy$Page:

    $pagePath: 'buyorder'
    transition: 'none'

    show_views: ()->
      if @views['info']
        @views['info'].el.remove()
      info = new $views.OrderInfo
      @$el.append info.el
      @views['info'] = info

    make_views: ()->
      top_banner: new $views.TopBanner {'title':'订单信息', 'nocart': true}






