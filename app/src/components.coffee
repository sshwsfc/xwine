$ma.view ()->

  # 主菜单
  MainTabs:
    tagName: 'ul'
    className: 'nav navbar-nav navbar-fixed-bottom'

    initialize : ()->
      @$('li a').each (id, el)=>
        nav = $(el).attr 'nav'
        $(el).bind 'click', ()=> @trigger 'nav', nav

    el : ()->
      el = document.createElement @tagName
      tabs = @options.tabs or {}
      $(el).addClass @className + ' num' + _.keys(tabs).length
      _.map tabs, (title, id)=>
        $(el).append "<li><a id='tab_#{id}' nav='#{id}'>#{title}</a></li>"
      el
    
    active: (nav)->
      @$('li').removeClass 'active'
      @$("li a#tab_#{nav}").parent().addClass 'active'

  # 子菜单
  SubTabs$MainTabs:
    className: 'nav nav-pills'

  Container:

    tagName: 'div'
    className: 'container'

    rended_views : false
 
    initialize: (options)->
      @bind 'beforeshow', @show_views, @
      @bind 'active', @active, @
  
    show_views: ()->
 
    active: ()->
      _.each @views, (v)->
        v.trigger 'active', v

    el : ()->
      el = document.createElement @tagName
      $el = $(el)
      if @attributes
        $el.attr @attributes

      views = @make_views()
      _.each views, (v)->
        $el.append v.el

      @views = views
      el

  # 包含菜单的组件
  TabContainer$Container:
    param_name: 'tab'

    initialize : (options)->
      $views.Container.prototype.initialize.call @, options
      @views.tab.bind 'nav', @totab, @

    totab: (nav)->
      page = @options.page
      params = page.params
      params[@param_name] = nav

      querystr = (for key, val of params
        "#{key}=#{val}").join '&'

      page.navigate "#{page.pageUri}?#{querystr}", on

    make_views: ()->
      tab: new $views.MainTabs @options

    nav: (params)->
      tabnav = params[@param_name] or @options.default
      if not @options.tabs[tabnav]?
        tabnav = @options.default
      @views.tab.active tabnav

      if not @views[tabnav]?
        @views[tabnav] = @options.views(tabnav)
        @$el.append @views[tabnav].el
      
      @views[tabnav].trigger 'active', @
      @views[key].hide() for key,view of @views when key isnt tabnav and key isnt 'tab'
      @views[tabnav].show()

      if @views[tabnav].nav
        @views[tabnav].nav(params)

  SubTabContainer$TabContainer:
    param_name: 'subtab'

    make_views: ()->
      tab: new $views.SubTabs @options

  DataView:
    fetched: false

    initialize : ()->
      @colls.bind 'add', @addItem, @
      @colls.bind 'reset', @addAll, @
      @colls.bind 'all', @render, @

      @colls.bind 'start:sync', @startLoad, @
      @colls.bind 'finish:sync', @finishLoad, @

      @bind 'active', @fetch, @

    startLoad: (method, model, options)->
      @$loader and @$loader.show()

    finishLoad: (method, model, options)->
      @$loader and @$loader.hide()
      @fetched || (@fetched = true)

    finishFetch: (collection, resp)->
      //
    addItem: (model)->
      //
    addAll: ()->
      @clear()
      @colls.each @addItem, @
    clear: ()->
      //
    fetch: ()->
      @fetched || @colls.fetch {success: _.bind @finishFetch, @}

    refresh: ()->
      @clear()
      @colls.fetch {success: _.bind @finishFetch, @}

  ListView$DataView:

    tagName: 'div'
    className: 'ls'
    has_page: true
    end_text: '亲, 已经到最后了'
    more_text: '下一页更精彩哦'
    loading_text: '努力加载中..'
    list_tag: 'div'
    list_class: 'list-group'

    initialize : ()->
      $views.DataView.prototype.initialize.apply @, arguments

      @$ul = @$ '.items'
      @$loader = @$ 'div.loading'
      @$more = @$ 'a.next'

      @has_page = @has_page and @colls.nextPage?

      if @has_page
        @$more.click _.bind @nextPage, @
        @colls.bind 'page_end', @page_end, @
      else
        @$more.remove()

    el : (tagName, attributes, content)->
      el = document.createElement @tagName
      $(el).addClass @className
      $(el).html "<#{@list_tag} class='items #{@list_class}'></#{@list_tag}>
        <div class='row text-center'>
        <a class='next btn'>#{@more_text}</a></div>
        <div class='loading well text-center'>#{@loading_text}</div>"
      el

    clear: ()->
      @$ul.html ''

    addItem: (model)->
      @$ul.append @tmpl model.toData()

    finishFetch: (collection, resp)->
      @$loader.hide()
      if @has_page
        @$more.show()
      @fetched || (@fetched = true)

    startLoad: (method, model, options)->
      if @has_page
        @$more.hide()
      @$loader.show()

    page_end: ()->
      @$more.html @end_text
      @$more.unbind 'click'

    nextPage: ()->
      @colls.nextPage {success: _.bind @finishFetch, @}

