$ma.view ()->

  TopBanner:
    tagName: 'div'
    className: 'navbar navbar-inverse navbar-fixed-top'

    el : (tagName, attributes, content)->
      el = document.createElement @tagName
      $(el).addClass @className
      $(el).html '<a class="navbar-brand" href="#"></a>'

      if @options['nocart'] is undefined
        @cart = new $views.Cart {}
        $(el).prepend @cart.el

      if @options['title']?
        ($(el).find '.navbar-brand').text @options['title']

      el

    set_title: (title)->
      (@$ '.navbar-brand').text title

  TitleBar:
    tagName: 'div'
    className: 'title-bar'

  Cart:
    tagName: 'a'
    className: 'cart btn btn-info navbar-btn pull-right'

    initialize : ()->
      @_sup.initialize.apply @, arguments

      @colls = $.carts()
      @$count = @$ 'span.count'
      @count = 0

      @colls.bind 'add', @addItem, @
      @colls.bind 'reset', @addAll, @
      @colls.bind 'all', @render, @

      @addAll()

    addItem: (good)->
      @$count.text ++@count

    addAll: ()->
      @clear()
      @colls.each @addItem, @

    clear: ()->
      @$count.text 0

    el : ()->
      el = document.createElement @tagName
      $(el).addClass @className
      $(el).attr 'href', '#cart'
      $(el).html '购物车 <span class="count badge">0</span></a>'
      el

  HomeBanner$DataView:

    tagName: 'div'
    className: 'banner'
    colls: new $colls.Advs

    initialize : ()->
      @_sup.initialize.apply @, arguments

      @$el.append '<div class="loader" style="display:none;">loading...</div>'
      @$loader = @$ 'div.loader'

      @$ul = @$ 'ul#advs'
      @$nav = @$ 'ul.nav'

    el : ()->
      el = document.createElement @tagName
      $(el).addClass @className
      $(el).html '<div id="slider" class="swipe"><ul id="advs"></ul>
        <ul class="nav"></ul></div>'
      el
    clear: ()->
      @$ul.html ''

    addItem: (model)->
      ad = model.toData()
      li = $ "<li style='display: none;'><a href='#game?id=#{ad.product_id}&from=banner'><img src='#{ad.pic}'/></a></li>"
      @$ul.append li
      @$nav.append '<li></li>'

    finishFetch: (collection, resp)->
      (@$ul.find 'li:first-child').css 'display', 'block'
      (@$nav.find 'li:first-child').addClass 'selected'
      @swiper = new Swipe (@$ '#slider')[0], {
        speed: 400, auto: 5000, callback: (event, index, elem) =>
          (@$nav.find 'li').removeClass 'selected'
          $(@$nav.find('li')[index]).addClass 'selected'
      }

  Items$ListView:
    className: 'games'
    tmpl: $tmpls.game_item

  FeatureItems$Items:
    colls: new $colls.HomeItems

  CategoryItems$Items:

    initialize : ()->
      @colls = new $colls.CategoryItems
      $views.Items.prototype.initialize.apply @, arguments
      
    category: (cid)->
      @colls.category cid
      @refresh()

  Categorys$ListView:
    list_tag: 'ul'
    list_class: 'table col3'
    className: 'categorys'
    tmpl: $tmpls.category_item
    colls: new $colls.Categories

  CartGoods$DataView:
    tagName: 'div'
    className: 'goods'
    tmpl: $tmpls.good_item

    events:
      'click .clean_goods': 'cleanGoods'

    initialize : ()->
      @colls = $.carts()

      @$ul = @$ '.list-group'
      @$total = @$ '.total_amount'
      @total_amount = 0

      @colls.bind 'add', @addItem, @
      @colls.bind 'reset', @addAll, @
      @colls.bind 'all', @render, @
      @addAll()

      @bind 'active', @fetch, @

    el : (tagName, attributes, content)->
      el = document.createElement @tagName
      $(el).addClass @className
      $(el).html "<div class='list-group goods'></div>
        <div class='account_bar well well-small'><h5>总计: <span class='total_amount'></span>元</h5></div>
        <div class='row'>
        <div class='col col-sm-4'><a class='clean_goods btn btn-danger btn-block'>清空购物车</a></div>
        <div class='col col-sm-8'><a class='buy_order btn btn-success btn-block' href='#buyorder'>确认购买</a></div>
        </div>
        "
      el

    clear: ()->
      @$ul.html ''
      @total_amount = 0
      @$total.text @total_amount

    addItem: (model)->
      m = model.toData()
      @$ul.append @tmpl m
      @total_amount += m['price'] * m['count']
      @$total.text @total_amount

    cleanGoods: ()->
      @colls.clean()
    
  OrderInfo:
    events:
      'click #submit': 'submit'

    initialize : ()->
      @colls = $.carts()

      @$form = @$ 'form'
      @$success = @$ '.success'
      @$error = @$ '.error'

      @colls.bind 'submit.success', @success, @
      @colls.bind 'submit.error', @error, @

    el: ()->
      el = $ $tmpls.order_info {}
      el

    submit: ()->
      @colls.submit @$form.serialize()

    success: (data)->
      @$form.hide()
      @$success.show()
      @colls.clean()

    error: (err)->
      @$error.text err ? "提交订单错误，请填写完整信息再试。"
      @$error.show()

  SearchItems$Items:
    colls: new $colls.SearchItems

    search: (keyword)->
      @colls.search(keyword)
      @refresh()

  SearchBar:
    tagName: 'div'
    id: 'search_bar'
    className: 'subbar'

    initialize : ()->
      $views.Base.prototype.initialize.apply @, arguments
      @$input = @$ 'input'
      @$input.bind 'change', ()=> @trigger 'search', @$input.val()

    el : ()->
      el = document.createElement @tagName
      $(el).attr 'id', @id
      $(el).addClass @className
      $(el).html '<input type="search" placeholder="在这里输入文字"/>'
      el

  ItemView:

    tagName: 'div'
    id: 'game_info'
    events:
      "click .add_cart": "addcart"

    initialize : ()->
      @_sup.initialize.apply @, arguments
      @$icon = @$ '#info img'
      @$name = @$ '#info p b'
      @$ext_info = @$ '#info p.ext_info'
      @$infos = @$ '#infos'
      @$loader = @$ '.loading'
      @$content = @$ '#content'
      @$shop_cart = @$ '#shop_cart'
      @$buy_count = @$ '#buy_count'

    addcart: ()->
      $.carts().create
        project_id: @data['id']
        price: @data['price']
        price_id: @data['price_id']
        name: @data['name']
        icon: @data['icon']
        count: parseInt @$buy_count.val()

    set_game: (@game)->
      @clear()
      @$content.hide()
      @$loader.show()

    clear: ()->
      @lastScroll = 0
      @$icon.attr 'src', @game.get('icon') || ''
      @$name.html @game.get('name') || ''
      @$ext_info.html ''
      (@$ '#evaluator,#screen_shots').html ''

    finishFetch: (model, resp)->
      data = model.toData()
      ss = data['screenshots']
      desc = data['description'].replace /\n/g, '<br/>'

      @$name.html data['name']
      @$icon.attr 'src', data['icon_full']
      @$ext_info.html $tmpls.item_ext_info data
      
      infos = for name, value of data['infos']
        "<li><b>#{name}:</b>#{value}</li>"
      infos = infos.join ''
      (@$ '#infos').html "
        <ul>#{infos}</ul>
      "
      (@$ '#evaluator').html "
        <p>#{desc}</p>
      "
      sshtml = ["<ul class='gallery'>"]
      sslist = (_.map ss, (s)-> s.big).join ';'
      i = 0
      _.each ss, (s)->
        sshtml.push "<li><a href='#ss?img=#{s.big}'><img src='#{s.small}' index='#{i}'/></a></li>"
        i++
      sshtml.push '</ul>'
      (@$ '#screen_shots').html sshtml.join ''
      # (@$ '#screen_shots li img').click ()->
      #   index = new Number $(@).attr 'index'
      #   $.gallery sslist, index

      if data['price_id']
        @$shop_cart.show()

      @$loader.hide()
      @$content.show()
      @data = data

    el : ()->
      el = document.createElement @tagName
      $(el).attr 'id', @id
      $(el).html '
        <div id="info">
          <p><b></b></p>
          <img class="icon" src="" />
          <p class="ext_info"></p>
        </div>
        <div id="shop_cart" class="clearfix row" style="display: none;">
          <div class="col form-horizontal">
            <label class="pull-left control-label">购买数量</label>
            <input type="number" id="buy_count" value="1" class="col col-sm-4"/>
            <a class="add_cart btn btn-success pull-right">放入购物车</a>
          </div>
        </div>
        <div id="content" style="display: none;">
          <h4>详细参数</h4>
          <div id="infos"></div>
          <h4>美酒介绍</h4>
          <div id="evaluator"></div>
          <h4>图片展示</h4>
          <div id="screen_shots"></div>
        </div>
        <div class="loading" style="display: none;">游戏信息加载中...</div>'
      el

  ImageView:

    tagName: 'div'
    id: 'image'

    initialize : ()->
      @_sup.initialize.apply @, arguments
      @$img = @$ 'img'

    set_img: (url)->
      @$img.attr 'src', url

    el : ()->
      el = document.createElement @tagName
      $(el).attr 'id', @id
      $(el).html '<img src="" style="width: 100%;" class="screen_img"/>'
      el




